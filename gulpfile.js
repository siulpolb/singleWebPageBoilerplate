var gulp = require('gulp');
var pug = require('gulp-pug');
var sass = require('gulp-sass');
var minifyCSS = require('gulp-csso');
var concat = require('gulp-concat');
var sourcemaps = require('gulp-sourcemaps');

gulp.task('html', function(){
  return gulp.src('html/*.pug')
    .pipe(pug())
    .pipe(gulp.dest('build'))
});

gulp.task('css', function(){
  return gulp.src('css/**/*.scss')
    .pipe(sass())
    .pipe(minifyCSS())
    .pipe(gulp.dest('build/css'))
});

gulp.task('js', function(){
  return gulp.src('js/**/*.js')
    .pipe(sourcemaps.init())
    .pipe(concat('app.min.js'))
    //.pipe(sourcemaps.write())
    .pipe(gulp.dest('build/js'))
});

gulp.task('watch',function(){
  gulp.watch('js/**/*.js',['js']);
  gulp.watch('css/**/*.scss',['css']);
  gulp.watch('html/**/*.pug',['html']);
});

gulp.task('default', [ 'watch' ]);